/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { Events } = require("discord.js");

module.exports = {
  name: Events.InteractionCreate,
  async execute(message) {
    await message
      .reply(
        `🏓 Latency is ${Date.now() - message.createdTimestamp
        }ms. API Latency is ${Math.round(message.client.ws.ping)}ms`,
      );
  },
};
