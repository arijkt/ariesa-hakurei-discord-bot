/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { SlashCommandBuilder, EmbedBuilder, ButtonBuilder, ButtonStyle, ActionRowBuilder } = require('discord.js')
const config = require('../../../config.json')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('help')
        .setDescription('Display all supported commands'),
    async execute(message) {
        const embed = new EmbedBuilder()
            .setColor(0xEFFF00)
            .addFields(
                {
                    name: "Version",
                    value: "1.0.1"
                },
                {
                    name: 'General Commands',
                    value: config
                        .isCommands
                        .map(cmd => `${cmd.value} - ${cmd.description}`)
                        .join('\n')
                },
                {
                    name: 'Moderator Only',
                    value: config
                        .isMod
                        .map(cmd => `${cmd.value} - ${cmd.description}`)
                        .join('\n')
                },
                {
                    name: "from API",
                    value: config
                        .isEventCommands
                        .map(cmd => `${cmd.value} - ${cmd.description}`)
                        .join('\n')
                }
            )
            .setTimestamp()
            .setFooter({
                text: `Authored by: @${message.user.username}`
            })

        const invite = new ButtonBuilder()
            .setLabel('Changelog')
            .setURL('https://gitlab.com/arijkt/ariesa-hakurei-discord-bot/-/blob/master/changelog.md')
            .setStyle(ButtonStyle.Link)

        const row = new ActionRowBuilder()
            .addComponents(invite)

        await message
            .reply(
                {
                    embeds: [embed],
                    components: [row]
                })

    }
}