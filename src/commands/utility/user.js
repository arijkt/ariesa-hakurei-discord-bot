/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('user')
		.setDescription('Provides information about the user'),
	async execute(interaction) {
		// interaction.user is the object representing the User who ran the command
		// interaction.member is the GuildMember object, which represents the user in the specific guild
		await interaction
			.reply({
				content: `This command was run by ${interaction.user.username}, who joined on ${interaction.member.joinedAt}.`,
				ephemeral: true
			});
	},
};

