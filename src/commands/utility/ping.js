/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Provides latency information'),
	async execute(interaction) {
		await interaction
			.reply(`🏓Latency is ${Date.now() - interaction.createdTimestamp}ms. API Latency is ${Math.round(interaction.client.ws.ping)}ms`);
	},
};