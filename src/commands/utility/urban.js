/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
require('dotenv').config()

const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const { request } = require('undici');

const trim = (str, max) => (str.length > max ? `${str.slice(0, max - 3)}...` : str);

module.exports = {
    data: new SlashCommandBuilder()
        .setName('urban')
        .setDescription('Crowdsourced English-language online dictionary for slang words and phrases')
        .addStringOption(option =>
            option
                .setName('term')
                .setDescription('Find your term')),
    async execute(interaction) {
        if (!interaction.isChatInputCommand()) return;

        await interaction
            .deferReply();

        const term = interaction
            .options
            .getString('term');
        const query = new URLSearchParams({ term });
        const dictResult = await request(`${process.env.URBAN_API}?${query}`);
        const { list } = await dictResult
            .body
            .json();

        if (!list.length) {
            return interaction
                .editReply(`No results found for **${term}**.`);
        }

        // interaction.editReply(`**${term}**: ${list[0].definition}`);

        const [answer] = list;
        const embed = new EmbedBuilder()
            .setColor(0xEFFF00)
            .setTitle(answer.word)
            .setURL(answer.permalink)
            .addFields(
                {
                    name: 'Definition',
                    value: trim(answer.definition, 1024)
                },
                {
                    name: 'Example',
                    value: trim(answer.example, 1024)
                },
                {
                    name: 'Rating',
                    value: `${answer.thumbs_up} thumbs up. ${answer.thumbs_down} thumbs down.`,
                },
            );
        interaction
            .editReply({ embeds: [embed] });
    }
}