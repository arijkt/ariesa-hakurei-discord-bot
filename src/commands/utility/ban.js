/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { SlashCommandBuilder, PermissionFlagsBits, ButtonBuilder, ButtonStyle, ActionRowBuilder, time } = require('discord.js');
const { response } = require('express');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ban')
		.setDescription('Select a member and ban them')
		.addUserOption(option =>
			option
				.setName('target')
				.setDescription('The member to ban')
				.setRequired(true))
		.addStringOption(option =>
			option
				.setName('reason')
				.setDescription('The reason for banning'))
		.setDefaultMemberPermissions(PermissionFlagsBits.BanMembers)
		.setDMPermission(false),
	async execute(interaction) {
		const target = interaction
			.options
			.getUser('target');
		const reason = interaction
			.options
			.getString('reason') ?? 'No reason provided';

		const confirm = new ButtonBuilder()
			.setCustomId('confirm')
			.setLabel('Confirm Ban')
			.setStyle(ButtonStyle.Danger)
		const cancel = new ButtonBuilder()
			.setCustomId('cancel')
			.setLabel('Cancel')
			.setStyle(ButtonStyle.Secondary)

		const row = new ActionRowBuilder()
			.addComponents(confirm, cancel)

		const response = await interaction
			.reply({
				content: `Are you sure you want to ban **<@${target.id}>** for reason: ${reason} (auto-cancel for 20 seconds if no response)`,
				components: [row],
				ephemeral: true
			});

		const collectorFilter = i => i.user.id === interaction.user.id
		try {
			const confirmation = await response
				.awaitMessageComponent({
					filter: collectorFilter,
					time: 20_000
				})

			if (confirmation.customId === "confirm") {
				await interaction
					.guild
					.members
					.ban(target)
				await confirmation
					.update({
						content: `${target.username} has been banned for reason: ${reason}`,
						components: []
					})
			} else if (confirmation.customId === "cancel") {
				await confirmation.update({
					content: "Action cancelled",
					components: []
				})
			}
		} catch (error) {
			await interaction
				.editReply({
					content: 'Confirmation not received within 20 second, cancelling',
					components: []
				});
		}
	}
};