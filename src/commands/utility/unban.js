/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { SlashCommandBuilder, PermissionFlagsBits, ButtonBuilder, ButtonStyle, ActionRowBuilder } = require('discord.js');
const { response } = require('express');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('unban')
		.setDescription('Select a member and unban them')
		.addUserOption(option =>
			option
				.setName('target')
				.setDescription('The member to unban')
				.setRequired(true))
		.setDefaultMemberPermissions(PermissionFlagsBits.BanMembers)
		.setDMPermission(false),
	async execute(interaction) {
		const target = interaction
			.options
			.getUser('target');

		const confirm = new ButtonBuilder()
			.setCustomId('confirm')
			.setLabel('Confirm Unban')
			.setStyle(ButtonStyle.Danger)
		const cancel = new ButtonBuilder()
			.setCustomId('cancel')
			.setLabel('Cancel')
			.setStyle(ButtonStyle.Secondary)

		const row = new ActionRowBuilder()
			.addComponents(confirm, cancel)

		const response = await interaction
			.reply({
				content: `Are you sure you want to ban **<@${target.id}>**  \`auto-cancel for 20 seconds if no response\` `,
				components: [row],
				ephemeral: true
			});

		const collectorFilter = i => i.user.id === interaction.user.id
		try {
			const confirmation = await response
				.awaitMessageComponent({
					filter: collectorFilter,
					time: 8_000
				})

			if (confirmation.customId === "confirm") {
				await interaction
					.guild
					.members
					.unban(target)

				await confirmation
					.update({
						content: `${target.username} has been unbanned`,
						components: []
					})

			} else if (confirmation.customId === "cancel") {
				await confirmation.update({
					content: "Action cancelled",
					components: []
				})
			}
		} catch (error) {
			await interaction
				.editReply({
					content: `${error.status === 404 ? `WTF? You haven't banned <@${target.id}> LOL. Ban them and come back again.` : "Confirmation not received within 20 second, cancelling"}`,
					components: [],
				})
		}
	}
};