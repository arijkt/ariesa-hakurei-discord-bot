/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
const { Events } = require('discord.js');

module.exports = {
    name: Events.InteractionCreate,
    async execute(interaction) {
        if (!interaction.isChatInputCommand()) return;

        const { commandName } = interaction

        if (commandName === "stats") {
            return interaction
                .reply(`Server count: ${client.guilds.cache.size}`)
        }

        const command = interaction
            .client
            .commands
            .get(interaction.commandName);

        if (!command) {
            console
                .error(`No command matching ${interaction.commandName} was found.`);
            return;
        }

        try {
            await command.execute(interaction);
        } catch (error) {
            console
                .error(error);
            if (interaction.replied || interaction.deferred) {
                await interaction
                    .followUp({
                        content: 'There was an error while executing this command!', ephemeral: true
                    });
            } else {
                await interaction
                    .reply({
                        content: 'There was an error while executing this command!', ephemeral: true
                    });
            }
        }
    },
};
