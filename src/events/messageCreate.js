/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
require('dotenv').config()

const ping = require('../commands/prefix/ping.js');
const { Events } = require('discord.js');
const Keyv = require('keyv')

module.exports = {
    name: Events.MessageCreate,
    async execute(message) {

        const globalPrefix = process.env.KEYV_DATABASE
        const prefixes = new Keyv(process.env.KEYV_DATABASE);

        if (message.author.bot) return

        let args
        // handle messages in a guild
        if (message.guild) {
            let prefix

            if (message.content.startsWith(globalPrefix)) {
                prefix = globalPrefix
            } else {
                // check the guild-level prefix
                const guildPrefix = await prefixes.get(message.guild.id)
                if (message.content.startsWith(guildPrefix)) {
                    prefix = guildPrefix
                }
            }

            // if we found a prefix, setup args; otherwise, this isn't a command
            if (!prefix) return
            args = message
                .content
                .slice(prefix.length)
                .trim()
                .split(/\s+/)
        } else {
            // handle DMs
            const slice = message
                .content
                .startsWith(globalPrefix)
                ? globalPrefix.length
                : 0

            args = message
                .content
                .slice(slice)
                .split(/\s+/)

        }
        // get the first space-delimited argument after the prefix as the command
        const command = args
            .shift()
            .toLowerCase()


        // custom commands prefix
        if (command === 'prefix') {
            // if there's at least one argument, set the prefix
            if (args.length) {
                await prefixes
                    .set(message.guild.id, args[0]);
                return message
                    .channel
                    .send(`Successfully set prefix to \`${args[0]}\``);

            }
            return message
                .channel
                .send(`Prefix is \`${await prefixes.get(message.guild.id) || globalPrefix}\``)
        }

        if (command === 'ping') ping.execute(message)
        return message
            .channel
            .send('gk ada prefixnya mas')
    }
}
