/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
require('dotenv').config()

const { Client, Collection, GatewayIntentBits } = require('discord.js')
const path = require('node:path')
const express = require('express')
const fs = require('node:fs')
const app = express()

app.all('/', (req, res) => {
	res
		.send('Bot is running');
});

const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.MessageContent,
	]
});

client.commands = new Collection();
const foldersPath = path
	.join(__dirname, 'src/commands');
const commandFolders = fs
	.readdirSync(foldersPath);

for (const folder of commandFolders) {
	const commandsPath = path
		.join(foldersPath, folder);
	const commandFiles = fs
		.readdirSync(commandsPath)
		.filter(file => file
			.endsWith('.js'));
	for (const file of commandFiles) {
		const filePath = path
			.join(commandsPath, file);
		const command = require(filePath);
		if ('data' in command && 'execute' in command) {
			client
				.commands
				.set(command.data.name, command);
		} else {
			console
				.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

const eventsPath = path
	.join(__dirname, 'src/events');
const eventFiles = fs
	.readdirSync(eventsPath)
	.filter(file => file
		.endsWith('.js'));

for (const file of eventFiles) {
	const filePath = path
		.join(eventsPath, file);
	const event = require(filePath);
	if (event.once) {
		client
			.once(event.name, (...args) => event
				.execute(...args));
	} else {
		client
			.on(event.name, (...args) => event
				.execute(...args));
	}
}

// client.login(process.env.TOKEN).then(() => {
// 	client.user.setActivity('anime', {
// 		type: ActivityType
// 	});
// });

client.login(process.env.TOKEN).then(() => {
	client.user.setPresence({
		activities: [{
			name: 'Node.js with my GF'
		}],
		afk: true,
		status: 'dnd'
	})
});

app.listen(process
	.env
	.PORT || 5000, () => {
	console
		.log(`Express server is running on port ${process.env.PORT || 5000}`);
});
