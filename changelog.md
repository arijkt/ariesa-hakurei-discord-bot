# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

- 

## [1.0.2] - 2023-12-16 
### Added
- `/urban` - Crowdsourced English-language online dictionary for slang words and phrases
- `/help` - Show all commands
- `/kick` - Select member and kick them
- `/unban` - Select member and uban them
- Add validation (awaiting components) for Moderator Commands

### Fixed
- Fix typo and style on embeds


## [1.0.1] - 2023-12-14

### Added
- Registering slash commands
- `/user` - Provides information about the user
- `/ping` - Replies with "Pong" and provides latency information
- `/ban` - Select a member and ban them
- `/server` - Provides information about the server

<!-- Links -->
[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html

<!-- Versions -->
[unreleased]: https://gitlab.com/arijkt/ariesa-hakurei-discord-bot/-/blob/master/changelog.md?ref_type=heads&plain=0#unreleased
[1.0.2]: https://gitlab.com/arijkt/ariesa-hakurei-discord-bot/-/blob/master/changelog.md?ref_type=heads&plain=0#102-2023-12-16
[1.0.1]: https://gitlab.com/arijkt/ariesa-hakurei-discord-bot/-/blob/master/changelog.md?ref_type=heads&plain=0#101-2023-12-14