/**
 * Coding services provided by Arijkt <alharitsfaqih@gmail.com> 
 *  
 * Redistributing or reselling this code is prohibited.
 * This code is proprietary and confidential.
 *  
 * Authored by: 
 * Muhammad Faqih Alharits
 *  
 * For examples of work and collaboration inquiries, please visit:
 * https://gitlab.com/arijkt
 */
require('dotenv').config()

const { ShardingManager } = require('discord.js');

const manager = new ShardingManager('./bot.js', { token: process.env.TOKEN });

manager
    .on('shardCreate', shard => console
        .log(`Launched shard ${shard.id}`));

manager
    .spawn();
